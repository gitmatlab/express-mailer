# Express Mailer

A simple mailer for a website with a contact form and a booking form

## Installation

- Create your SSL certificates, so that you can run the server with https (see instructions below)
- create your .env file. Use the .env.default as template.

```ssh
npm i
npm start
```

## Resources

### CORS

- https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
- expressjs cors proxy: https://medium.com/@dtkatz/3-ways-to-fix-the-cors-error-and-how-access-control-allow-origin-works-d97d55946d9

- expressjs plugins: cors morgan and helmet: https://blog.logrocket.com/express-middleware-a-complete-guide/#setting-up-an-expressjs-api

### SSL

[Running expressjs over https](https://timonweb.com/javascript/running-expressjs-server-over-https/)

create your certificates for local development:

```shell
openssl req -x509 -out localhost.crt -keyout localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```
