const mailer = require("./mailer");
const mailCompiler = require("./mail-compiler");

describe("Send Mail", () => {
  test("Sending workes", async () => {
    const form = {
      apiKey: "1234",
      firstName4: "Janette",
      lastName4: "Noways",
      comment: "Hello World!\n\r",
    };

    const mailSubject = "🚴🏿‍♂️ Booking Request 🏄";
    const mailText = mailCompiler.loadAndCreate("booking-request.mustache", {
      form,
    });
    const messageId = await mailer.send({ mailSubject, mailText });
    expect(messageId).toBeDefined();
  }, 60000);
});
