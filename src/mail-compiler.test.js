const mailCompiler = require("./mail-compiler");

describe("mail-compiler", () => {
  test("Template works", () => {
    const template = "Hello {{name}}";
    const data = { name: "Bo" };
    const text = mailCompiler.create(template, data);
    expect(text).toBe("Hello Bo");
  });

  test("Text contains date", () => {
    const template = "{{date}}";
    const data = {};
    const text = mailCompiler.create(template, data);
    // Sun Sep 18 2022 18:12:58 GMT+0200 (Central European Summer Time)
    expect(text.match("GMT")).toBeTruthy();
  });

  test("Text contains form data", () => {
    const template = "{{form}}";
    const data = {
      form: { locale: "en" },
    };
    const text = mailCompiler.create(template, data);
    expect(text.match("locale: en")).toBeTruthy();
  });

  test("Use booking template file", () => {
    const templateName = "booking-request.mustache";
    const data = {
      form: { locale: "en" },
    };
    const text = mailCompiler.loadAndCreate(templateName, data);
    expect(text.match("Buchung")).toBeTruthy();
    expect(text.match("GMT")).toBeTruthy();
    expect(text.match("locale: en")).toBeTruthy();
    // console.log(text);
  });

  test("Text removes carriage returns", () => {
    const template = "{{form}}";
    const data = {
      form: {
        comment: "Hello,\r\n" + "\r\n" + "I'm sorry to trouble you\r\n",
      },
    };
    const text = mailCompiler.create(template, data);
    expect(text.match("Hello")).toBeTruthy();
    expect(text.indexOf("\\r")).toBe(-1);
    expect(text.indexOf("\n\n")).toBe(-1);
  });

  test("Use contact template file", () => {
    const templateName = "contact.mustache";
    const data = {
      form: { locale: "en", name: "Lisa Müller" },
    };
    const text = mailCompiler.loadAndCreate(templateName, data);
    expect(text.match("Nachricht")).toBeTruthy();
    expect(text.match("GMT")).toBeTruthy();
    expect(text.match("locale: en")).toBeTruthy();
    // console.log(text);
  });
});
