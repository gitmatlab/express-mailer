require("dotenv").config();

const throng = require("throng");
const fs = require('fs')
const https = require('https')

const privateKey = fs.readFileSync(process.env.KEY, "utf8");
const certificate = fs.readFileSync(process.env.CRT, "utf8");
const credentials = { key: privateKey, cert: certificate };
const port = process.env.SERVER_PORT;

const worker = (id) => {
  const app  = require("./app");
  const httpsServer = https.createServer(credentials, app);

  httpsServer.listen(port, () => {
    console.log(`Listening at https://localhost:${port}`);
    console.log(`Check health on: curl -k https://localhost:${port}/health`);
  });
};

throng({ worker, count: 1 });
