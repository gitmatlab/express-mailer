"use strict";
require("dotenv").config();
const nodemailer = require("nodemailer");

// async.await is not allowed in global scope, must use a wrapper
exports.send = async ({
  mailSubject = "Formular",
  mailText = "No Message",
  mailHTML = "",
}) => {
  console.log("Sending mail to %s", process.env.RECIPIENT);
  try {
    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: process.env.SMTP_SECURE, // true for 465, false for other ports
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
      },
    });
    let info = await transporter.sendMail({
      from: `Website <info@dasilva-surfcamp.de>`,
      to: process.env.RECIPIENT, // list of receivers
      subject: mailSubject,
      text: mailText,
    });
    if (mailHTML) info.html = mailHTML;

    console.log("Message sent: %s", info.messageId);
    return info.messageId;
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};
