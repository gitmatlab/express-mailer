require("dotenv").config();
const app = require("./app");
const supertest = require("supertest");
const mailer = require("./mailer");

let server;

beforeAll(() => {
  const port = process.env.SERVER_PORT;
  server = app.listen(port);

  // silence console output
  // jest.spyOn(console, "log").mockImplementation(() => undefined);
});

afterAll(() => {
  server.close();
});

const request = supertest(app);
const apiKey = process.env.API_KEY;

describe("App Contoller", () => {
  it("/health (GET)", () => {
    return request.get("/health").expect(200).expect("OK");
  });

  it("/api/contact (POST)", () => {
    // mock mailer
    jest.spyOn(mailer, "send").mockImplementation(() => Promise.resolve());
    const body = { apiKey };
    const redirectPath = process.env.CONTACT_REDIRECT;
    return request
      .post("/api/contact")
      .send(body)
      .expect(302)
      .then((res) => {
        console.log(res.text);
        expect(res.text).toContain(redirectPath);
      });
  });

  it("/api/contact (POST) mock error", () => {
    // mock error
    jest.spyOn(mailer, "send").mockImplementation(() => Promise.reject());
    const body = { apiKey };
    return request.post("/api/contact").send(body).expect(500);
  });

  it("/api/contact (POST) wrong API Key", () => {
    const body = { apiKey: "wrong" };
    return request
      .post("/api/contact")
      .send(body)
      .expect(401)
      .then((res) => {
        expect(res.error.text).toContain("Invalid API Key");
      });
  });

  it("/api/contact (POST) form data", () => {
    // mock mailer
    jest.spyOn(mailer, "send").mockImplementation(() => Promise.resolve());
    const redirectPath = process.env.CONTACT_REDIRECT;
    return request
      .post("/api/contact")
      .send("apiKey=1234") // works only with express.urlencoded()
      .expect(302)
      .then((res) => {
        console.log(res.text);
        expect(res.text).toContain(redirectPath);
      });
  });

  it("/api/booking-request (POST)", () => {
    // mock mailer
    jest.spyOn(mailer, "send").mockImplementation(() => Promise.resolve());
    const body = { apiKey };
    const redirectPath = process.env.BOOKING_REDIRECT;
    return request
      .post("/api/booking-request")
      .send(body)
      .expect(302)
      .then((res) => {
        console.log(res.text);
        expect(res.text).toContain(redirectPath);
      });
  });

  it("/api/booking-request (POST)", () => {
    // mock error
    jest.spyOn(mailer, "send").mockImplementation(() => Promise.reject());
    const body = { apiKey };
    return request.post("/api/contact").send(body).expect(500);
  });
});
