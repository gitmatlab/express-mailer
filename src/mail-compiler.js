const yaml = require('yaml')
const mustache = require('mustache')
const fs = require('fs')
require('dotenv').config()

exports.create = (template = "", data = {}) => {
  let templateData = {
    date: new Date(),
    ...data,
  }

  if (data.form) templateData.form = formDataAsString(data.form)

  const text = mustache.render(template, templateData)
  return text
}

exports.load = (templateName, path = process.env.TEMPLATES) => {
  try {
    return fs.readFileSync(`${path}/${templateName}`, 'utf8')
  } catch (error) {
    console.error("Failed to load template", templateName, error)
    return null
  }
}

exports.loadAndCreate = (templateName, data) => {
  const template = this.load(templateName)
  const mailText = this.create(template, data)
  return mailText
}

function formDataAsString(formData) {
  let formString = yaml.stringify(formData)
  formString = formString.replaceAll('\\r', '')
  formString = formString.replaceAll('\n\n', '\n')
  return formString
}