require("dotenv").config();

const express = require("express");
const cors = require("cors");
const helmet = require("helmet");

const mailer = require("./mailer");
const mailCompiler = require("./mail-compiler");
const apiKeys = [process.env.API_KEY];

const app = express();

app.use(cors());
app.options("*", cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded());
app.use(setHeaders);

app.get("/health", (req, res) => {
  res.status(200).send("OK");
});

app.use(errorIfInvalidAPIKey);

// route handlers
app.post("/api/booking-request", async (req, res, next) => {
  const body = req.body;
  res.locals.mailSubject = "🚴🏿‍♂️ Booking Request 🏄";
  res.locals.mailText = mailCompiler.loadAndCreate("booking-request.mustache", {
    form: body,
  });
  res.locals.redirect =
    process.env.BOOKING_REDIRECT || `booking-request/response/`;
  next();
});

app.post("/api/contact", async (req, res, next) => {
  res.locals.mailSubject = "🔔 Nachricht 🔔";
  res.locals.mailText = mailCompiler.loadAndCreate("contact.mustache", {
    form: req.body,
  });
  res.locals.redirect =
    process.env.CONTACT_REDIRECT || `booking-request/response/`;
  next();
});

app.use(sendMail);

// middleware
async function sendMail(req, res, next) {
  try {
    await mailer.send({
      mailSubject: res.locals.mailSubject,
      mailText: res.locals.mailText,
    });
    const origin = req.headers.origin || process.env.CORS_ORIGIN;
    const locale = req.body.locale || process.env.DEFAULT_LOCALE;
    const redirect = res.locals.redirect;
    return res.redirect(`${origin}/${locale}/${redirect}`);
  } catch (error) {
    console.log(error);
    next(500);
  }
}

function errorIfInvalidAPIKey(req, res, next) {
  const key = req.body?.apiKey;
  if (!key) {
    console.error("400 API Key Required", key);
    console.error("Body", req.body);
    return res.status(400).send(`API Key Required`);
  } else if (!~apiKeys.indexOf(key)) {
    console.error("401 Invalid API Key", key);
    console.error("Body", req.body);
    return res.status(401).send(`Invalid API Key`);
  } else {
    next();
  }
}

function setHeaders(req, res, next) {
  res.header("Access-Control-Allow-Origin", `${process.env.CORS_ORIGIN}`);
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
}

module.exports = app;
